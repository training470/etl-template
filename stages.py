from coroutine import coroutine


def extract_data(batch, *args, **kwargs):
    """
    ЭТО ПЕРВЫЙ МЕТОД КОРУТИНЫ (ИДЕТ БЕЗ ДЕКОРАТОРА).
    :param batch: функция, в которую будет передана пачка данных
    :return:
    """

    # Здесь мы извлекаем что-либо из какого-либо источника данные
    datas = (i for i in range(10))

    for data in datas:
        print("extract", data)
        batch.send(data)


@coroutine
def transform_data(batch, *args, **kwargs):
    """
    Например преобразование результата sql-запроса в json-объект
    :return:
    """

    while True:
        data = yield
        json_data = {data: data ** 2}
        print("transform", data, "->", json_data)
        batch.send(json_data)  # можно передать кортеж из нескольких объектов


@coroutine
def load_data(*args, **kwargs):
    """
    например отправка json-объекта на чей-нибудь api-сервис.
    Последний метод корутины ничего не посылает
    :return:
    """

    while True:
        data = yield
        print("load", data)
