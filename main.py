from stages import load_data, transform_data, extract_data

if __name__ == '__main__':
    load = load_data()
    transform = transform_data(load)
    extract_data(transform)
